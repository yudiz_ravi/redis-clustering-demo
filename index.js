// cd /opt/homebrew/etc
// redis-server redis.conf 
const fs = require('fs')
const Redis = require('ioredis')
const nodes = [
    {
        host: 'localhost',
        port: 7000
    },
    {
        host: 'localhost',
        port: 7001
    },
    {
        host: 'localhost',
        port: 7002
    },
    {
        host: 'localhost',
        port: 7003
    },
    {
        host: 'localhost',
        port: 7004
    },
    {
        host: 'localhost',
        port: 7005
    }
]
const redisClient2 = new Redis.Cluster(nodes, {
    scaleReads: 'slave',
})

redisClient2.on('error', function (error) {
    console.log('Error in Redis2', error)
    process.exit(1)
})

const checkScriptExists = async function (scriptName, hash) {
    try {
        const script = fs.readFileSync(`luaScripts/${scriptName}`, 'utf8')
  
        const data = await redisClient2.function('LOAD', script)
        console.log('data :: ', data)
        return data
    } catch (error) {
        console.log('Error in Redis checkScriptExists', error)
    }
}

redisClient2.on('connect', function () {
    console.log('redis connected.....')
    checkScriptExists('function.lua', '')
})



function addKeys() {
    redisClient2.set('foo0', 'bar0')
    redisClient2.set('foo1', 'bar1')
    redisClient2.set('foo2', 'bar2')
    redisClient2.set('foo3', 'bar3')
    redisClient2.set('foo4', 'bar4')
}

async function callLuaScript() {
    try {
        const keys = await redisClient2.keys('*')
        console.log('keys', keys)
        const data = await redisClient2.fcall('del_keys', 0, [])
        console.log('lua script result :: ', data)
    } catch (error) {
        console.log('Error while calling lua script ', error)
    }
}

addKeys()
setTimeout(() => {
    callLuaScript()
}, 10000)
