#!lua name=mylib
local function remove_keys()
    local cursor="0";
    local count = 0;
    repeat
    local scanResult = redis.call("SCAN", cursor, "MATCH", '*', "COUNT", 100);
        local keys = scanResult[2];
        for i = 1, #keys do
            local key = keys[i];
            redis.call("DEL", key);
            count = count +1;
        end;
        cursor = scanResult[1];
    until cursor == "0";
    return "Total "..count.." keys Deleted" ;
end

local function del_keys()
    local result = remove_keys()
    return result
end


redis.register_function('del_keys', del_keys)
